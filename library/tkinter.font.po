# Copyright (C) 2001-2018, Python Software Foundation
# For licence information, see README file.
#
msgid ""
msgstr ""
"Project-Id-Version: Python 3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-09-23 16:16+0200\n"
"PO-Revision-Date: 2023-07-08 10:48+0200\n"
"Last-Translator: Christophe Nanteuil <christophe.nanteuil@gmail.com>\n"
"Language-Team: FRENCH <traductions@lists.afpy.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.2.2\n"

#: library/tkinter.font.rst:2
msgid ":mod:`tkinter.font` --- Tkinter font wrapper"
msgstr ":mod:`tkinter.font` — enveloppe pour les polices *Tkinter*"

#: library/tkinter.font.rst:8
msgid "**Source code:** :source:`Lib/tkinter/font.py`"
msgstr "**Code source :** :source:`Lib/tkinter/font.py`"

#: library/tkinter.font.rst:12
msgid ""
"The :mod:`tkinter.font` module provides the :class:`Font` class for creating "
"and using named fonts."
msgstr ""
"Le module :mod:`tkinter.font` fournit la classe :class:`Font` pour créer et "
"utiliser des polices nommées."

#: library/tkinter.font.rst:15
msgid "The different font weights and slants are:"
msgstr "Les différentes épaisseurs et inclinaisons des polices sont :"

#: library/tkinter.font.rst:24
msgid ""
"The :class:`Font` class represents a named font. *Font* instances are given "
"unique names and can be specified by their family, size, and style "
"configuration. Named fonts are Tk's method of creating and identifying fonts "
"as a single object, rather than specifying a font by its attributes with "
"each occurrence."
msgstr ""
"La classe :class:`Font` représente une police nommée. Les instances *Font* "
"reçoivent des noms uniques et peuvent être spécifiées par leur configuration "
"de famille, de taille et de style. Les polices nommées sont la méthode de "
"*Tk* pour créer et identifier les polices comme un seul objet, plutôt que de "
"spécifier une police par ses attributs à chaque occurrence."

#: library/tkinter.font.rst:30
msgid "arguments:"
msgstr "arguments :"

#: library/tkinter.font.rst:0
msgid "*font* - font specifier tuple (family, size, options)"
msgstr "*font* – *n*-uplet spécificateur de police (famille, taille, options)"

#: library/tkinter.font.rst:0
msgid "*name* - unique font name"
msgstr "*name* – nom de police unique"

#: library/tkinter.font.rst:0
msgid "*exists* - self points to existing named font if true"
msgstr ""
"*exists* – s'il est vrai, *self* pointe vers la police nommée existante"

#: library/tkinter.font.rst:36
msgid "additional keyword options (ignored if *font* is specified):"
msgstr "options nommées supplémentaires (ignorées si *font* est spécifié) :"

#: library/tkinter.font.rst:0
msgid "*family* - font family i.e. Courier, Times"
msgstr "*family* – famille de polices, c'est-à-dire *Courier*, *Times*"

#: library/tkinter.font.rst:0
msgid "*size* - font size"
msgstr "*size* – taille de la police"

#: library/tkinter.font.rst:0
msgid "If *size* is positive it is interpreted as size in points."
msgstr "Si *size* est positif, il est interprété comme une taille en points."

#: library/tkinter.font.rst:0
msgid "If *size* is a negative number its absolute value is treated"
msgstr "Si *size* est un nombre négatif sa valeur absolue est traitée"

#: library/tkinter.font.rst:0
msgid "as size in pixels."
msgstr "comme taille en pixels."

#: library/tkinter.font.rst:0
msgid "*weight* - font emphasis (NORMAL, BOLD)"
msgstr "*weight* – accentuation de la police (*NORMAL*, *BOLD* pour gras)"

#: library/tkinter.font.rst:0
msgid "*slant* - ROMAN, ITALIC"
msgstr "*slant* – *ROMAN* pour romain, *ITALIC* pour italique"

#: library/tkinter.font.rst:0
msgid "*underline* - font underlining (0 - none, 1 - underline)"
msgstr "*underline* – soulignement de la police (0 – aucun, 1 – souligné)"

#: library/tkinter.font.rst:0
msgid "*overstrike* - font strikeout (0 - none, 1 - strikeout)"
msgstr "*overstrike* – police barrée (0 – aucune, 1 – barré)"

#: library/tkinter.font.rst:50
msgid "Return the attributes of the font."
msgstr "Renvoie les attributs de la police."

#: library/tkinter.font.rst:54
msgid "Retrieve an attribute of the font."
msgstr "Récupère un attribut de la police."

#: library/tkinter.font.rst:58
msgid "Modify attributes of the font."
msgstr "Modifie les attributs de la police."

#: library/tkinter.font.rst:62
msgid "Return new instance of the current font."
msgstr "Renvoie une nouvelle instance de la police actuelle."

#: library/tkinter.font.rst:66
msgid ""
"Return amount of space the text would occupy on the specified display when "
"formatted in the current font. If no display is specified then the main "
"application window is assumed."
msgstr ""
"Renvoie la quantité d'espace que le texte occuperait sur l'affichage "
"spécifié s'il était formaté dans la police actuelle. Si *displayof* n'est "
"pas spécifié, *tk* suppose que c'est la fenêtre principale de l'application."

#: library/tkinter.font.rst:72
msgid "Return font-specific data. Options include:"
msgstr "Renvoie des données spécifiques à la police. Les options incluent :"

#: library/tkinter.font.rst:76
msgid "*ascent* - distance between baseline and highest point that a"
msgstr ""
"*ascent* – distance entre la ligne de base et le point le plus haut qu'un"

#: library/tkinter.font.rst:79
msgid "character of the font can occupy"
msgstr "caractère de la police peut occuper"

#: library/tkinter.font.rst:79
msgid "*descent* - distance between baseline and lowest point that a"
msgstr ""
"*descent* – distance entre la ligne de base et le point le plus bas qu'un"

#: library/tkinter.font.rst:82
msgid "*linespace* - minimum vertical separation necessary between any two"
msgstr "*linespace* – séparation verticale minimale nécessaire entre deux"

#: library/tkinter.font.rst:82
msgid "characters of the font that ensures no vertical overlap between lines."
msgstr ""
"caractères de la police qui assure l'absence de chevauchement vertical entre "
"les lignes."

#: library/tkinter.font.rst:84
msgid "*fixed* - 1 if font is fixed-width else 0"
msgstr "*fixed* – 1 si la police est à largeur fixe sinon 0"

#: library/tkinter.font.rst:88
msgid "Return the different font families."
msgstr "Renvoie les différentes familles de polices."

#: library/tkinter.font.rst:92
msgid "Return the names of defined fonts."
msgstr "Renvoie les noms des polices définies."

#: library/tkinter.font.rst:96
msgid "Return a :class:`Font` representation of a tk named font."
msgstr "Renvoie une représentation :class:`Font` d'une police *tk* nommée."

# suit un :
#: library/tkinter.font.rst:98
msgid "The *root* parameter was added."
msgstr "le paramètre *root* a été ajouté."
