# Copyright (C) 2001-2018, Python Software Foundation
# For licence information, see README file.
#
msgid ""
msgstr ""
"Project-Id-Version: Python 3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-07-23 14:38+0200\n"
"PO-Revision-Date: 2023-06-17 10:31+0200\n"
"Last-Translator: Christophe Nanteuil <christophe.nanteuil@gmail.com>\n"
"Language-Team: FRENCH <traductions@lists.afpy.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.2.2\n"

#: library/asyncio-subprocess.rst:7
msgid "Subprocesses"
msgstr "Sous-processus"

#: library/asyncio-subprocess.rst:9
msgid ""
"**Source code:** :source:`Lib/asyncio/subprocess.py`, :source:`Lib/asyncio/"
"base_subprocess.py`"
msgstr ""
"**Code source :** :source:`Lib/asyncio/subprocess.py`, :source:`Lib/asyncio/"
"base_subprocess.py`"

#: library/asyncio-subprocess.rst:14
msgid ""
"This section describes high-level async/await asyncio APIs to create and "
"manage subprocesses."
msgstr ""
"Cette section décrit des API de haut niveau de *asyncio* pour créer et gérer "
"des sous-processus via ``async``/``await``."

#: library/asyncio-subprocess.rst:19
msgid ""
"Here's an example of how asyncio can run a shell command and obtain its "
"result::"
msgstr ""
"Voici un exemple de comment *asyncio* peut lancer une commande shell et "
"obtenir son résultat ::"

#: library/asyncio-subprocess.rst:40
msgid "will print::"
msgstr "affiche ::"

#: library/asyncio-subprocess.rst:46
msgid ""
"Because all asyncio subprocess functions are asynchronous and asyncio "
"provides many tools to work with such functions, it is easy to execute and "
"monitor multiple subprocesses in parallel.  It is indeed trivial to modify "
"the above example to run several commands simultaneously::"
msgstr ""
"Puisque toutes les fonctions à sous-processus d'*asyncio* sont synchrones et "
"qu'*asyncio* fournit de nombreux outils pour travailler avec de telles "
"fonctions, il est facile d'exécuter et de surveiller de nombreux processus  "
"en parallèle. Il est en effet trivial de modifier l'exemple ci-dessus pour "
"exécuter plusieurs commandes simultanément ::"

#: library/asyncio-subprocess.rst:58
msgid "See also the `Examples`_ subsection."
msgstr "Voir également la section `Exemples`_."

#: library/asyncio-subprocess.rst:62
msgid "Creating Subprocesses"
msgstr "Créer des sous-processus"

#: library/asyncio-subprocess.rst:67
msgid "Create a subprocess."
msgstr "Crée un sous-processus."

#: library/asyncio-subprocess.rst:69 library/asyncio-subprocess.rst:87
#, fuzzy
msgid ""
"The *limit* argument sets the buffer limit for :class:`StreamReader` "
"wrappers for :attr:`Process.stdout` and :attr:`Process.stderr` (if :const:"
"`subprocess.PIPE` is passed to *stdout* and *stderr* arguments)."
msgstr ""
"Le paramètre *limit* définit la taille maximale du tampon pour les instances "
"de :class:`StreamReader` encapsulant :attr:`Process.stdout` et :attr:"
"`Process.stderr` (Si :attr:`subprocess.PIPE` est passé aux paramètres "
"*stdout* et *stderr*)."

#: library/asyncio-subprocess.rst:73 library/asyncio-subprocess.rst:91
msgid "Return a :class:`~asyncio.subprocess.Process` instance."
msgstr "Renvoie une instance de :class:`~asyncio.subprocess.Process`."

#: library/asyncio-subprocess.rst:75
msgid ""
"See the documentation of :meth:`loop.subprocess_exec` for other parameters."
msgstr ""
"Voir la documentation de :meth:`loop.subprocess_exec` pour d'autres "
"paramètres."

# suit un :
#: library/asyncio-subprocess.rst:78 library/asyncio-subprocess.rst:105
msgid "Removed the *loop* parameter."
msgstr "suppression du paramètre *loop*."

#: library/asyncio-subprocess.rst:85
msgid "Run the *cmd* shell command."
msgstr "Exécute la commande *cmd* dans un *shell*."

#: library/asyncio-subprocess.rst:93
msgid ""
"See the documentation of :meth:`loop.subprocess_shell` for other parameters."
msgstr ""
"Voir la documentation de :meth:`loop.subprocess_shell` pour d'autres "
"paramètres."

#: library/asyncio-subprocess.rst:98
msgid ""
"It is the application's responsibility to ensure that all whitespace and "
"special characters are quoted appropriately to avoid `shell injection "
"<https://en.wikipedia.org/wiki/Shell_injection#Shell_injection>`_ "
"vulnerabilities. The :func:`shlex.quote` function can be used to properly "
"escape whitespace and special shell characters in strings that are going to "
"be used to construct shell commands."
msgstr ""
"Il est de la responsabilité de l'application de s'assurer que tous les "
"espaces et les caractères spéciaux sont correctement mis entre guillemets "
"pour éviter les vulnérabilités de type `injection de code <https://en."
"wikipedia.org/wiki/Shell_injection#Shell_injection>`_. La fonction :func:"
"`shlex.quote` peut être utilisée pour l’échappement des espaces et "
"caractères spéciaux dans les chaînes utilisées pour construire des commandes "
"shell."

# suit un :
#: library/asyncio-subprocess.rst:110
msgid ""
"Subprocesses are available for Windows if a :class:`ProactorEventLoop` is "
"used. See :ref:`Subprocess Support on Windows <asyncio-windows-subprocess>` "
"for details."
msgstr ""
"les sous-processus sont disponibles pour Windows si un :class:"
"`ProactorEventLoop` est utilisé. Voir :ref:`Support des sous-processus sous "
"Windows <asyncio-windows-subprocess>` pour plus de précisions."

#: library/asyncio-subprocess.rst:116
msgid ""
"asyncio also has the following *low-level* APIs to work with subprocesses: :"
"meth:`loop.subprocess_exec`, :meth:`loop.subprocess_shell`, :meth:`loop."
"connect_read_pipe`, :meth:`loop.connect_write_pipe`, as well as the :ref:"
"`Subprocess Transports <asyncio-subprocess-transports>` and :ref:`Subprocess "
"Protocols <asyncio-subprocess-protocols>`."
msgstr ""
"*asyncio* propose aussi les API de « bas niveau » suivantes pour travailler "
"avec les sous-processus : :meth:`loop.subprocess_exec`, :meth:`loop."
"subprocess_shell`, :meth:`loop.connect_read_pipe`, :meth:`loop."
"connect_write_pipe`, ainsi que les :ref:`asyncio-subprocess-transports` et :"
"ref:`asyncio-subprocess-protocols`."

#: library/asyncio-subprocess.rst:124
msgid "Constants"
msgstr "Constantes"

#: library/asyncio-subprocess.rst:129
msgid "Can be passed to the *stdin*, *stdout* or *stderr* parameters."
msgstr "Peut être passé aux paramètres  *stdin*, *stdout* ou *stderr*."

#: library/asyncio-subprocess.rst:131
msgid ""
"If *PIPE* is passed to *stdin* argument, the :attr:`Process.stdin <asyncio."
"subprocess.Process.stdin>` attribute will point to a :class:`StreamWriter` "
"instance."
msgstr ""
"Si *PIPE* est passé au paramètre *stdin*, l'attribut :attr:`Process.stdin "
"<asyncio.subprocess.Process.stdin>` ne pointera pas vers une instance de :"
"class:`StreamWriter`."

#: library/asyncio-subprocess.rst:135
msgid ""
"If *PIPE* is passed to *stdout* or *stderr* arguments, the :attr:`Process."
"stdout <asyncio.subprocess.Process.stdout>` and :attr:`Process.stderr "
"<asyncio.subprocess.Process.stderr>` attributes will point to :class:"
"`StreamReader` instances."
msgstr ""
"Si *PIPE* est passé au paramètre *stdout* ou *stderr*, l'attribut :attr:"
"`Process.stdout <asyncio.subprocess.Process.stdout>` et :attr:`Process."
"stderr <asyncio.subprocess.Process.stderr>` pointeront vers des instances "
"de :class:`StreamReader`."

#: library/asyncio-subprocess.rst:143
msgid ""
"Special value that can be used as the *stderr* argument and indicates that "
"standard error should be redirected into standard output."
msgstr ""
"Une valeur spéciale qui peut être passée au paramètre *stderr* et qui "
"indique que la sortie d'erreur doit être redirigée vers la sortie standard."

#: library/asyncio-subprocess.rst:149
msgid ""
"Special value that can be used as the *stdin*, *stdout* or *stderr* argument "
"to process creation functions.  It indicates that the special file :data:`os."
"devnull` will be used for the corresponding subprocess stream."
msgstr ""
"Une valeur spéciale qui peut être passée à l'argument *stdin*, *stdout* ou "
"*stderr* des fonctions créant des processus. Elle implique l'utilisation du "
"fichier :data:`os.devnull` pour le flux correspondant du processus."

#: library/asyncio-subprocess.rst:155
msgid "Interacting with Subprocesses"
msgstr "Interagir avec les sous-processus"

#: library/asyncio-subprocess.rst:157
msgid ""
"Both :func:`create_subprocess_exec` and :func:`create_subprocess_shell` "
"functions return instances of the *Process* class.  *Process* is a high-"
"level wrapper that allows communicating with subprocesses and watching for "
"their completion."
msgstr ""
"Les fonctions :func:`create_subprocess_exec` et :func:"
"`create_subprocess_shell` renvoient des instances de la classe *Process*. "
"*Process* est une enveloppe de haut niveau qui permet de communiquer avec "
"les sous-processus et de surveiller leur achèvement."

#: library/asyncio-subprocess.rst:165
msgid ""
"An object that wraps OS processes created by the :func:"
"`create_subprocess_exec` and :func:`create_subprocess_shell` functions."
msgstr ""
"Objet qui encapsule les processus du système d'exploitation créés par les "
"fonctions :func:`create_subprocess_exec` et :func:`create_subprocess_shell`."

#: library/asyncio-subprocess.rst:169
msgid ""
"This class is designed to have a similar API to the :class:`subprocess."
"Popen` class, but there are some notable differences:"
msgstr ""
"Cette classe est conçue pour avoir une API similaire à la classe :class:"
"`subprocess.Popen`, mais il existe quelques différences notables :"

#: library/asyncio-subprocess.rst:173
msgid ""
"unlike Popen, Process instances do not have an equivalent to the :meth:"
"`~subprocess.Popen.poll` method;"
msgstr ""
"contrairement à *Popen*, les instances Process n'ont pas d'équivalent à la "
"méthode :meth:`~subprocess.Popen.poll` ;"

#: library/asyncio-subprocess.rst:176
msgid ""
"the :meth:`~asyncio.subprocess.Process.communicate` and :meth:`~asyncio."
"subprocess.Process.wait` methods don't have a *timeout* parameter: use the :"
"func:`~asyncio.wait_for` function;"
msgstr ""
"les méthodes :meth:`~asyncio.subprocess.Process.communicate` et :meth:"
"`~asyncio.subprocess.Process.wait` n'ont pas de paramètre *timeout* : "
"utilisez la fonction :func:`~asyncio.wait_for` ;"

#: library/asyncio-subprocess.rst:180
msgid ""
"the :meth:`Process.wait() <asyncio.subprocess.Process.wait>` method is "
"asynchronous, whereas :meth:`subprocess.Popen.wait` method is implemented as "
"a blocking busy loop;"
msgstr ""
":meth:`asyncio.subprocess.Process.wait` est asynchrone, tandis que la "
"méthode :meth:`subprocess.Popen.wait` est implémentée comme une boucle "
"bloquante ;"

#: library/asyncio-subprocess.rst:184
msgid "the *universal_newlines* parameter is not supported."
msgstr "le paramètre *universal_newlines* n'est pas pris en charge."

#: library/asyncio-subprocess.rst:186
msgid "This class is :ref:`not thread safe <asyncio-multithreading>`."
msgstr ""
"Cette classe n'est :ref:`pas conçue pour un contexte multi-fils <asyncio-"
"multithreading>`."

#: library/asyncio-subprocess.rst:188
msgid ""
"See also the :ref:`Subprocess and Threads <asyncio-subprocess-threads>` "
"section."
msgstr ""
"Voir aussi la section :ref:`sous-processus et fils d'exécution <asyncio-"
"subprocess-threads>`."

#: library/asyncio-subprocess.rst:193
msgid "Wait for the child process to terminate."
msgstr "Attend que le sous processus s'arrête."

#: library/asyncio-subprocess.rst:195
msgid "Set and return the :attr:`returncode` attribute."
msgstr "Définit et renvoie l'attribut :attr:`returncode`."

# suit un :
#: library/asyncio-subprocess.rst:199
msgid ""
"This method can deadlock when using ``stdout=PIPE`` or ``stderr=PIPE`` and "
"the child process generates so much output that it blocks waiting for the OS "
"pipe buffer to accept more data. Use the :meth:`communicate` method when "
"using pipes to avoid this condition."
msgstr ""
"cette méthode peut générer un interblocage quand ``stdout=PIPE`` ou "
"``stderr=PIPE`` est utilisé et que le sous-processus génère tellement de "
"sorties qu'il se bloque, dans l'attente que le tampon du tube côté OS "
"accepte des données supplémentaires. Pour éviter cette situation, choisissez "
"la méthode :meth:`communicate` quand vous utilisez des tubes."

#: library/asyncio-subprocess.rst:207
msgid "Interact with process:"
msgstr "Interagit avec le processus :"

#: library/asyncio-subprocess.rst:209
msgid "send data to *stdin* (if *input* is not ``None``);"
msgstr "envoie des données sur le *stdin* (si *input* n'est pas ``None``) ;"

#: library/asyncio-subprocess.rst:210
msgid "read data from *stdout* and *stderr*, until EOF is reached;"
msgstr ""
"lit les données sur *stdout* et *stderr*, jusqu'à ce que le EOF soit "
"atteint ;"

#: library/asyncio-subprocess.rst:211
msgid "wait for process to terminate."
msgstr "attend que le processus s'arrête."

#: library/asyncio-subprocess.rst:213
msgid ""
"The optional *input* argument is the data (:class:`bytes` object) that will "
"be sent to the child process."
msgstr ""
"Le paramètre optionnel *input* (objet de type :class:`bytes`) représente les "
"données transmises au sous-processus."

#: library/asyncio-subprocess.rst:216
msgid "Return a tuple ``(stdout_data, stderr_data)``."
msgstr "Renvoie un *n*-uplet ``(stdout_data, stderr_data)``."

#: library/asyncio-subprocess.rst:218
msgid ""
"If either :exc:`BrokenPipeError` or :exc:`ConnectionResetError` exception is "
"raised when writing *input* into *stdin*, the exception is ignored.  This "
"condition occurs when the process exits before all data are written into "
"*stdin*."
msgstr ""
"Si l'une des exceptions :exc:`BrokenPipeError` ou :exc:"
"`ConnectionResetError` est levée lors de l'écriture de *input* dans *stdin*, "
"l'exception est ignorée. Cette condition se produit lorsque le processus se "
"termine avant que toutes les données ne soient écrites dans *stdin*."

#: library/asyncio-subprocess.rst:223
msgid ""
"If it is desired to send data to the process' *stdin*, the process needs to "
"be created with ``stdin=PIPE``.  Similarly, to get anything other than "
"``None`` in the result tuple, the process has to be created with "
"``stdout=PIPE`` and/or ``stderr=PIPE`` arguments."
msgstr ""
"Si vous souhaitez envoyer des données au processus *stdin*, le processus "
"doit être créé avec ``stdin=PIPE``. De même, pour obtenir autre chose que "
"``None`` dans le *n*-uplet résultat, le processus doit être créé avec les "
"arguments ``stdout=PIPE`` et/ou ``stderr=PIPE``."

#: library/asyncio-subprocess.rst:229
msgid ""
"Note, that the data read is buffered in memory, so do not use this method if "
"the data size is large or unlimited."
msgstr ""
"Notez que les données lues sont mises en cache en mémoire, donc n'utilisez "
"pas cette méthode si la taille des données est importante voire illimitée."

#: library/asyncio-subprocess.rst:234
msgid "Sends the signal *signal* to the child process."
msgstr "Envoie le signal *signal* au sous-processus."

# suit un :
#: library/asyncio-subprocess.rst:238
msgid ""
"On Windows, :py:data:`SIGTERM` is an alias for :meth:`terminate`. "
"``CTRL_C_EVENT`` and ``CTRL_BREAK_EVENT`` can be sent to processes started "
"with a *creationflags* parameter which includes ``CREATE_NEW_PROCESS_GROUP``."
msgstr ""
"sous Windows, :py:data:`SIGTERM` est un alias pour :meth:`terminate`. "
"*CTRL_C_EVENT* et *CTRL_BREAK_EVENT* peuvent être envoyés aux processus "
"démarrés avec un paramètre *creationflags* incluant "
"``CREATE_NEW_PROCESS_GROUP``."

#: library/asyncio-subprocess.rst:245
msgid "Stop the child process."
msgstr "Arrête le sous-processus."

#: library/asyncio-subprocess.rst:247
#, fuzzy
msgid ""
"On POSIX systems this method sends :py:const:`signal.SIGTERM` to the child "
"process."
msgstr ""
"Sur les systèmes POSIX, cette méthode envoie :py:data:`signal.SIGTERM` au "
"sous-processus."

#: library/asyncio-subprocess.rst:250
msgid ""
"On Windows the Win32 API function :c:func:`TerminateProcess` is called to "
"stop the child process."
msgstr ""
"Sous Windows, la fonction API Win32 :c:func:`TerminateProcess` est appelée "
"pour arrêter le processus enfant."

#: library/asyncio-subprocess.rst:255
msgid "Kill the child process."
msgstr "Arrête le sous-processus."

#: library/asyncio-subprocess.rst:257
msgid ""
"On POSIX systems this method sends :py:data:`SIGKILL` to the child process."
msgstr ""
"Sur les systèmes POSIX, cette méthode envoie :py:data:`signal.SIGTERM` au "
"sous-processus."

#: library/asyncio-subprocess.rst:260
msgid "On Windows this method is an alias for :meth:`terminate`."
msgstr "Sous Windows, cette méthode est un alias pour :meth:`terminate`."

#: library/asyncio-subprocess.rst:264
msgid ""
"Standard input stream (:class:`StreamWriter`) or ``None`` if the process was "
"created with ``stdin=None``."
msgstr ""
"Flux d'entrée standard (:class:`StreamWriter`) ou ``None`` si le processus a "
"été créé avec ``stdin=None``."

#: library/asyncio-subprocess.rst:269
msgid ""
"Standard output stream (:class:`StreamReader`) or ``None`` if the process "
"was created with ``stdout=None``."
msgstr ""
"Flux de sortie standard (:class:`StreamReader`) ou ``None`` si le processus "
"a été créé avec ``stdout=None``."

#: library/asyncio-subprocess.rst:274
msgid ""
"Standard error stream (:class:`StreamReader`) or ``None`` if the process was "
"created with ``stderr=None``."
msgstr ""
"Flux d'erreur standard (:class:`StreamReader`) ou ``None`` si le processus a "
"été créé avec ``stderr=None``."

# suit un :
#: library/asyncio-subprocess.rst:279
msgid ""
"Use the :meth:`communicate` method rather than :attr:`process.stdin.write() "
"<stdin>`, :attr:`await process.stdout.read() <stdout>` or :attr:`await "
"process.stderr.read() <stderr>`. This avoids deadlocks due to streams "
"pausing reading or writing and blocking the child process."
msgstr ""
"utilisez la méthode :meth:`communicate` plutôt que :attr:`process.stdin."
"write() <stdin>`, :attr:`wait process.stdout.read() <stdout>` ou :attr:`wait "
"process.stderr.read() <stderr>`. Cela évite les interblocages dus aux flux "
"qui interrompent la lecture ou l'écriture et bloquent le processus enfant."

#: library/asyncio-subprocess.rst:288
msgid "Process identification number (PID)."
msgstr ""
"Numéro d'identification du processus (PID, pour *Process Identification "
"Number* en anglais)."

#: library/asyncio-subprocess.rst:290
msgid ""
"Note that for processes created by the :func:`create_subprocess_shell` "
"function, this attribute is the PID of the spawned shell."
msgstr ""
"Notez que pour les processus créés par la fonction :func:"
"`create_subprocess_shell`, cet attribut est le PID du shell généré."

#: library/asyncio-subprocess.rst:295
msgid "Return code of the process when it exits."
msgstr "Code de retour du processus quand il se termine."

#: library/asyncio-subprocess.rst:297
msgid "A ``None`` value indicates that the process has not terminated yet."
msgstr "Une valeur ``None`` indique que le processus n'est pas encore terminé."

#: library/asyncio-subprocess.rst:299
msgid ""
"A negative value ``-N`` indicates that the child was terminated by signal "
"``N`` (POSIX only)."
msgstr ""
"Une valeur négative ``-N`` indique que le sous-processus a été terminé par "
"le signal ``N`` (seulement sur les systèmes *POSIX*)."

#: library/asyncio-subprocess.rst:306
msgid "Subprocess and Threads"
msgstr "Sous-processus et fils d'exécution"

#: library/asyncio-subprocess.rst:308
msgid ""
"Standard asyncio event loop supports running subprocesses from different "
"threads by default."
msgstr ""
"La boucle d'événement asynchrone standard prend en charge l'exécution de "
"sous-processus à partir de différents fils d'exécution par défaut."

#: library/asyncio-subprocess.rst:311
msgid ""
"On Windows subprocesses are provided by :class:`ProactorEventLoop` only "
"(default), :class:`SelectorEventLoop` has no subprocess support."
msgstr ""
"Sous Windows, les sous-processus sont fournis par :class:`ProactorEventLoop` "
"uniquement (par défaut), :class:`SelectorEventLoop` ne prend pas en charge "
"les sous-processus."

#: library/asyncio-subprocess.rst:314
msgid ""
"On UNIX *child watchers* are used for subprocess finish waiting, see :ref:"
"`asyncio-watchers` for more info."
msgstr ""
"Sous UNIX, les *observateurs d'enfants (child watchers)* sont utilisés pour "
"l'attente de fin de sous-processus, voir :ref:`asyncio-watchers` pour plus "
"d'informations."

#: library/asyncio-subprocess.rst:320
msgid ""
"UNIX switched to use :class:`ThreadedChildWatcher` for spawning subprocesses "
"from different threads without any limitation."
msgstr ""
"UNIX est passé à l'utilisation de :class:`ThreadedChildWatcher` pour générer "
"des sous-processus à partir de différents threads sans aucune limitation."

#: library/asyncio-subprocess.rst:323
msgid ""
"Spawning a subprocess with *inactive* current child watcher raises :exc:"
"`RuntimeError`."
msgstr ""
"Instancier un sous-processus avec un observateur enfant actuel *inactif* "
"lève l'exception :exc:`RuntimeError`."

#: library/asyncio-subprocess.rst:326
msgid ""
"Note that alternative event loop implementations might have own limitations; "
"please refer to their documentation."
msgstr ""
"Notez que ces implémentations alternatives de la boucle d'événements peuvent "
"comporter leurs propres limitations. Veuillez vous référer à leur "
"documentation."

# suit un :
#: library/asyncio-subprocess.rst:331
msgid ""
"The :ref:`Concurrency and multithreading in asyncio <asyncio-"
"multithreading>` section."
msgstr ""
"la section :ref:`Exécution concurrente et multi-fils d'exécution <asyncio-"
"multithreading>`."

#: library/asyncio-subprocess.rst:336
msgid "Examples"
msgstr "Exemples"

#: library/asyncio-subprocess.rst:338
msgid ""
"An example using the :class:`~asyncio.subprocess.Process` class to control a "
"subprocess and the :class:`StreamReader` class to read from its standard "
"output."
msgstr ""
"Un exemple utilisant la classe :class:`~asyncio.subprocess.Process` pour "
"contrôler un sous-processus et la classe :class:`StreamReader` pour lire sa "
"sortie standard."

#: library/asyncio-subprocess.rst:344
msgid ""
"The subprocess is created by the :func:`create_subprocess_exec` function::"
msgstr ""
"Le sous-processus est créé par la fonction :func:`create_subprocess_exec` ::"

#: library/asyncio-subprocess.rst:371
msgid ""
"See also the :ref:`same example <asyncio_example_subprocess_proto>` written "
"using low-level APIs."
msgstr ""
"Voir également :ref:`le même exemple <asyncio_example_subprocess_proto>`, "
"écrit en utilisant des API de bas niveau."
